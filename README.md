# Kicad Template Project Repository

## About

This repository contains template project for KiCAD 8. It also uses 
[my KiCAD library](https://gitlab.com/osinski/kicad-library).

**NOTE:** following instructions assume you use Linux.


## How to use

### Prerequisites

1. KiCAD version 8
1. [Interactive BOM](https://github.com/openscopeproject/InteractiveHtmlBom)
if you want to use ``make interactive-bom``
**NOTE:** this assumes you have it installed with PIP and `generate_interactive_bom` command is available
1. [PcbDraw](https://github.com/yaqwsx/PcbDraw)
if you want to use ``make pcb-drawings``


### Instructions

1. create fork of this repository
1. [delete fork relationship](https://docs.gitlab.com/ee/user/project/repository/forking_workflow.html#unlink-a-fork)
1. clone your repo (remember to add ``--recurse-submodules`` while invoking ``git clone``) 
1. make sure *init-project.sh* and *pre-commit-hook* are executable
1. run ``./init-project.sh`` specifying author and project name (**NOTE:** the script assumes that 
tile is a single word, so use separators (e.g. *-*) instead of spaces if name has multiple words):
    ```shell
    ./init-project.sh --author 'Marcin Osiński' --title my-kicad-project
    ```
1. open the project in KiCAD and enjoy
